<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Traits\ApiReturnFormatTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TransportController extends Controller
{
    use ApiReturnFormatTrait;

    public function calculate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transport_means'   => ['required', 'string', Rule::in(['car', 'truck', 'lorry', 'boat', 'ship', 'speed_boat'])],
            'distance'          => ['required', 'numeric', 'min:1'],
            'product_weight'    => ['nullable', 'numeric', 'min:1'],
        ]);

        if ($validator->fails())
        {
            return $this->responseWithError(__('Validation Error'), $validator->errors()->first(), 422);
        }

        $transportMeans     = $request->input('transport_means');
        $distance           = $request->input('distance');
        $productWeight      = $request->input('product_weight') ?? 1;

        //transport means
        $staticTransportMeans = [
            'car',
            'truck',
            'lorry',
            'boat',
            'ship',
            'speed_boat',
        ];

        // costs for different transport means
        $costs = [
            'car'           => 5,       // Cost per kilometer for car transport
            'truck'         => 15,      // Cost per kilometer for truck transport
            'lorry'         => 20,      // Cost per kilometer for lorry transport
            'boat'          => 1000,    // Cost for boat transport (entire journey)
            'ship'          => 2000,    // Cost for ship transport (entire journey)
            'speed_boat'    => 1500,    // Cost for speed boat transport (entire journey)
        ];

        $totalCost = 0;

        if (array_key_exists($transportMeans, $costs)) {
            if (in_array($transportMeans, ['car', 'truck', 'lorry'])) {
                $totalCost = $costs[$transportMeans] * $distance * $productWeight;
            } else {
                $totalCost = $costs[$transportMeans];
            }
        }

        return $this->responseWithSuccess(__('Price calculated successfully'), [
            'totalCost'         => $totalCost,
            'transportMeans'    => $transportMeans,
            'distance'          => $distance,
            'productWeight'     => $productWeight,
        ]);
    }
}
