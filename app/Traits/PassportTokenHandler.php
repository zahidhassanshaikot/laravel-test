<?php

namespace App\Traits;

use App\Models\User;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Passport;
use Laravel\Passport\RefreshTokenRepository;
use Laravel\Passport\Client as OClient;
use GuzzleHttp\Client;

trait PassportTokenHandler
{
    /**
     * @param $user
     * @param $remember_me
     * @return array
     * @desc Create a new personal access token for the user
     */
    protected function createOnlyToken(User $user, $remember_me = false): array
    {
        // Create token
        $tokenData = $user->createToken('Personal Access Token');
        $accessToken = $tokenData->accessToken;

        if ($remember_me) {
            // Update expire time
            $token = $tokenData->token;
            $token->expires_at = now()->addWeeks(1);
            $token->save();
        }

        $expiration = $tokenData->token->expires_at->diffInSeconds(now());

        return [
            'access_token' => $accessToken,
            'token_type'   => 'Bearer',
            'expires_in'   => $expiration
        ];
    }

    /**
     * @param $usernameOrEmail
     * @param $password
     * @param $scope
     * @return PromiseInterface|Response
     * @throws \Throwable
     */
    public function accessAndRefreshToken($usernameOrEmail, $password, $scope = '')
    {
        try {
            // If password grant client id
        $oClient = OClient::where('password_client', 1)->first();
        throw_if(!$oClient, new \Exception());

            // Send api request
            $baseUrl    = url('/');

            $http       = new Client([
                'verify' => false,
            ]);
            $response = $http->request('POST', "{$baseUrl}/oauth/token", [
            'form_params' => [
                'grant_type'    => 'password',
                'client_id'     => $oClient->id,
                'client_secret' => $oClient->secret,
                'username'      => $usernameOrEmail,
                'password'      => $password,
                'scope'         => $scope
            ],
        ]);

        return json_decode((string) $response->getBody(), true);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $refresh_token
     * @return Response
     * @throws \Throwable
     */
    public function refreshToken($refresh_token)
    {
        try {
            $oClient = OClient::where('password_client', 1)->first();
            throw_if(!$oClient, new \Exception());

            // Send api request
            $baseUrl = url('/');
            $http       = new Client([
                'verify' => false,
            ]);
            $response = $http->request('POST', "{$baseUrl}/oauth/token", [
                'form_params' => [
                    'refresh_token' => $refresh_token,
                    'client_id'     => $oClient->id,
                    'client_secret' => $oClient->secret,
                    'grant_type'    => 'refresh_token'
                ],
            ]);
            return json_decode((string) $response->getBody(), true);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $user
     * @return bool
     * @throws \Exception
     */
    protected function revokeAccessAndRefreshToken($user): bool
    {
        try {
            $token = $user->token();

            /* --------------------------- revoke access token -------------------------- */
            $token->revoke();
            $token->delete();

            /* -------------------------- revoke refresh token -------------------------- */
            $refreshTokenRepository = app(RefreshTokenRepository::class);
            $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($token->id);

            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $user
     * @param $ttl
     * @return array
     */
    protected function tokenForLimitedTime($user, $ttl = 15)
    {
        // Set token expire time
        Passport::tokensExpireIn(now()->addMinutes($ttl));
        Passport::refreshTokensExpireIn(now()->addMinutes($ttl));
        Passport::personalAccessTokensExpireIn(now()->addMinutes($ttl));

        // Generate access token
        return $this->createOnlyToken($user);
    }
}
