# Please follow the instructions

`Server Requirements:` Php server & CLI version >= 8.1 <br>


`step 1:` clone this git repository <br>
`step 2:` copy .env.example to .env then config database info<br>
`step 3:` run command <code>composer install</code> <br>
`step 4:` run command <code>php artisan migrate</code><br>
`step 5:` run command <code>php artisan db:seed</code> <br>
`step 6:` run command <code>php artisan key:generate</code> <br>
`step 7:` run command <code>php artisan passport:install</code> <br>
# Authors of this repo

### Regards [Zahid Hassan Shaikot](https://github.com/zahidhassanshaikot)
