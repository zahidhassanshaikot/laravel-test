<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccessTokenResource;
use App\Http\Resources\AccessWithRefreshTokenResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\UserService;
use App\Traits\ApiReturnFormatTrait;
use App\Traits\PassportTokenHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Auth\AuthenticationException;
use DB;

class ApiAuthController extends Controller
{
    use ApiReturnFormatTrait,PassportTokenHandler;


    public function register (Request $request): JsonResponse {
        try {
            $validator = Validator::make($request->all(), [
                'name'            => ['required', 'string', 'max:100'],
                'email'                 => ['required', 'string', 'email', 'max:190', Rule::unique('users')],
                'password'              => ['required', 'string', 'min:8', 'confirmed'],
                'password_confirmation' => ['required', 'string', 'min:8'],
                'phone'                 => ['nullable', 'max:25', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
                'address'               => ['nullable', 'string', 'max:255'],

            ]);

            if ($validator->fails())
            {
                return $this->responseWithError(__('Validation Error'), $validator->errors()->first(), 422);
            }
            $data                    = $request->all();
            $data['password']        = Hash::make($data['password']);
            $data['remember_token']     = Str::random(10);
            $data['email_verified_at']  = now();
            $user                       = User::create($data);

            auth()->login($user);
            $token = new AccessTokenResource($this->createOnlyToken(auth()->user()));

            return $this->responseWithSuccess(__('You have been successfully registered!'),$token);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(),[],500);
        }
    }

    public function login (Request $request): JsonResponse {
        try {
            $validator = Validator::make($request->all(), [
                'email'         => 'required|string|email|max:190',
                'password'      => 'required|string|min:8',
            ]);
            if ($validator->fails()){
                return $this->responseWithError(__('Validation Error'), $validator->errors(), 422);
            }
            $user = User::where('email', $request->email)->first();

            if ($user) {

                if (Hash::check($request->password, $user->password)) {
                    $credentials = collect($request->all())->except(['remember_me'])->toArray();
                    $remember_me = collect($request->all())->only('remember_me');
                    if (!auth()->attempt($credentials, $remember_me)) {
                        throw new AuthenticationException(trans('auth.failed'));
                    }

                    $token          = $this->createOnlyToken(auth()->user(), $remember_me);
                    $data           = new AccessTokenResource($token);
                    return $this->responseWithSuccess(__('You have been successfully logged in!'),$data);
                } else {
                    return $this->responseWithError(__('Invalid Credentials'), [],422);
                }
            } else {
                return $this->responseWithError(__('User not found.'),[],404);
            }
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(),[],500);
        }
    }

    public function logout (Request $request): JsonResponse {
        try{
            $this->revokeAccessAndRefreshToken(auth()->user());
            return $this->responseWithSuccess(__('You have been successfully logged out!'));
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(),[],500);
        }
    }

    public function authUser(): JsonResponse
    {
        try {
            return $this->responseWithSuccess(__('Logged in user info.'), new UserResource(auth()->user()));
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(),[],500);
        }
    }

    public function loginWithRefreshToken(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'email'         => 'required|string|email|max:190',
                'password'      => 'required|string|min:8',
            ]);
            if ($validator->fails()){
                return $this->responseWithError(__('Validation Error'), $validator->errors(), 422);
            }
            $response = new AccessWithRefreshTokenResource($this->accessAndRefreshToken($request['email'], $request['password']));

            return $this->responseWithSuccess(__('You have been successfully logged in!'), $response);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(),[],500);
        }
    }

    public function refreshTokenPost(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'refresh_token' => 'required'
            ]);
            if ($validator->fails()){
                return $this->responseWithError(__('Validation Error'), $validator->errors(), 422);
            }

            $response = new AccessWithRefreshTokenResource($this->refreshToken($request['refresh_token']));

            return $this->responseWithSuccess(__('Token refreshed successfully!'), $response);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(),[],500);
        }
    }
}
