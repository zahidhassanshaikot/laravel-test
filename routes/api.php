<?php

use App\Http\Controllers\API\V1\TransportController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\Auth\ApiAuthController;

Route::fallback(function (){
    return response()->json([
        'status'    => false,
        'message'   => 'API resource not found',
        'data'      => []
        ], 404);
});

Route::prefix('v1')->middleware(['cors', 'json.response'])->group(function () {
    Route::get('login', function () {
        return response()->json([
            'status'    => false,
            'message'   => 'Unauthorized',
            'data'      => []
            ], 401);
    })->name('api.login');

    //Authentication
    Route::post('register', [ApiAuthController::class, 'register']);
    Route::post('login', [ApiAuthController::class, 'login']);
    Route::post('login-with-refresh-token', [ApiAuthController::class, 'loginWithRefreshToken']);
    Route::post('refresh-token', [ApiAuthController::class, 'refreshTokenPost']);


    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', [ApiAuthController::class, 'logout']);
        Route::get('auth-user', [ApiAuthController::class, 'authUser']);

        // Calculate transport price
        Route::post('calculate', [TransportController::class, 'calculate']);

    });
});

