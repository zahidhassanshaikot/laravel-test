<?php

namespace App\Traits;

use App\Models\User;

trait ApiReturnFormatTrait
{

    protected function responseWithSuccess($message = '', $data = [], $code = 200)
    {
        $response = [
            'status'        => true,
            'message'       => $message == '' ? __('Success') : $message,
            'data'          => $data,
        ];

        return response()->json($response, $code);

    }

    protected function responseWithError($message = '', $data = [], $code = null)
    {
        if ($code == null) {
            $code = 400;
        }
        $response = [
            'status'        => false,
            'message'       => $message == '' ? __('Success') : $message,
            'data'          => $data,
        ];

        return response()->json($response, $code);
    }
}
